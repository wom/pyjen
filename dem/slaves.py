import logging
from jenkins import LAUNCHER_SSH
from jenkins import Jenkins as jen
from jenkins import JenkinsException
from pprint import pformat

l = logging.getLogger('dem')


def add_slave_rest(jen: jen, new_slave):
    from time import gmtime, strftime
    cur_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    # if new_slave.launcher == 'SSH':
    params = {
        'host': new_slave.host,
        'port': '22',
        'username': new_slave.credential,
        'credentialsID': new_slave.credential
    }

    jen.create_node(
        new_slave.name,
        nodeDescription='my test slave',
        remoteFS=f'/Users/mchris/.jenkins/fake_roots/{new_slave.name}/',
        exclusive=True,
        launcher=LAUNCHER_SSH,
        launcher_params=params,
        labels=new_slave.labels
    )
    l.info('Slave Created')



def add_slave_groovy(jen: jen, new_slave):
    # Create Groovy script to generate a slave.
    groovy_create = f'''
    import hudson.model.*;
    import jenkins.model.*;
    import hudson.slaves.*;
    import hudson.slaves.EnvironmentVariablesNodeProperty.Entry;
    import hudson.plugins.sshslaves.SSHLauncher;

    import hudson.plugins.sshslaves.verifiers.*;

    SshHostKeyVerificationStrategy hostKeyVerificationStrategy = new KnownHostsFileKeyVerificationStrategy()

    ComputerLauncher launcher = new SSHLauncher(
        "{new_slave.host}",       //hostname
        22,                       // port
        "{new_slave.credential}", // creds
        (String)null,  (String)null,  (String)null,  (String)null,  (Integer)null, (Integer)null, (Integer)null,
        hostKeyVerificationStrategy
    )

    Slave agent = new DumbSlave(
            "{new_slave.name}",
            "/Users/mchris/.jenkins/fake_roots/{new_slave.name}/remoteFS",
            launcher)
    agent.nodeDescription = "Agent node description"
    agent.numExecutors = 2
    agent.labelString = "{new_slave.labels}"
    agent.mode = Node.Mode.NORMAL
    agent.retentionStrategy = new RetentionStrategy.Always()

    // Create a "Permanent Agent"
    Jenkins.instance.addNode(agent)
    println('Success')
    '''

    result = jen.run_script(groovy_create)
    if result.rstrip() != 'Success':
        raise f'{new_slave} Groovy Create Failure. {result}'
    l.info('Slave Created')


def audit_slaves(server: jen, slaves, debug=True):
    """
    :param server: Jenkins server
    :param slaves: Slaves that (should) exist
    """
    # setup_logging('info')

    nodes = [node for node in server.get_nodes() if node['name'] != 'master']
    for slave in slaves:
        if slave.name not in [node['name'] for node in nodes]:
            l.info('We need to create {}'.format(slave))
            if not debug:
                try:
                    add_slave_rest(server, slave)
                except JenkinsException as e:
                    l.info(f'Failed to create slave via REST with {e}, retrying through script console..')
                    add_slave_groovy(server, slave)


if __name__ == "__main__":
    pass
