#!/usr/bin/env bash -e
echo 'Setting up venv - requires Python 3.6'
# assumption:  python3.6 installed
#   sudo apt-get install python3.6 
#   brew install python 3.6
python3.6 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
