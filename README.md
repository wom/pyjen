# Requirements
* python3.6
* Jenkins setup (Admin account with administrative privs)
# Setup
```bash
# venv
git clone git@bitbucket.org:wom/pyjen.git
cd pyjen 
./setup.sh

# setup yaml
sed -i 's/$JENKINS_USER/USER/ settings.yaml
sed -i 's/$JENKINS_USER_TOKEN/USER_TOKEN/ settings.yaml
```
# Run
```bash
(venv) mchris.w0maIr.pyjen>python PyJen.py -h
usage: PyJen.py [-h] [--version]
                [-s [{jobs,plugins,slaves} [{jobs,plugins,slaves} ...]]]
                [-l {CRITICAL,ERROR,WARNING,INFO,DEBUG}] [-d]

Process some integers.

optional arguments:
  -h, --help            show this help message and exit
  --version             show program version
  -s [{jobs,plugins,slaves} [{jobs,plugins,slaves} ...]], --steps [{jobs,plugins,slaves} [{jobs,plugins,slaves} ...]]
                        Steps to perform.
  -l {CRITICAL,ERROR,WARNING,INFO,DEBUG}, --loglevel {CRITICAL,ERROR,WARNING,INFO,DEBUG}
                        Log Level
  -d, --debug
```
# Packages Used
* [Jenkins](https://jenkins.io/download/ "Jenkins") [(docs)](https://jenkins.io/doc/)
* [Pipelines](https://plugins.jenkins.io/workflow-aggregator "Pipelines") [(docs)](https://jenkins.io/doc/book/pipeline/)
* [Python Jenkins](https://pypi.python.org/pypi/python-jenkins "Python Jenkins") [(docs)](http://python-jenkins.readthedocs.io/en/latest/ "")
