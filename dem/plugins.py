import logging
from jenkins import LAUNCHER_SSH, NotFoundException
from jenkins import Jenkins as jen
from pprint import pformat, pprint
import json
from time import sleep

l = logging.getLogger('dem')


def audit_plugins(jen: jen, plugins, debug=True):
    """
    :param jen: Jenkins server connection
    :param plugins: Plugins to install
    :return: Bool on whether restart was performed
    """
    force_restart = False
    for plugin in plugins:
        install = True
        restart_needed = False
        for key, value in jen.get_plugins().items():
            if plugin.name in key and plugin.version == value['version']:
                install = False
        if install:
            l.info(f'Updating Plugin: {plugin}')
            if not debug: restart_needed = jen.install_plugin(plugin.name, include_dependencies=True)
        if restart_needed:
            force_restart = True

    if force_restart:
        l.warning('Notice: Will restart Jenkins to finalize Plugin installation in 5s..')
        sleep(5)
        jen.run_script('Jenkins.instance.restart()')
        sleep(10)
    return True if force_restart else False


if __name__ == "__main__":
    pass
