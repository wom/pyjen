import logging
from jenkins import LAUNCHER_SSH, NotFoundException
from jenkins import Jenkins as jen
import os, sys
from pprint import pformat

l = logging.getLogger('dem')


def _job_exists(jen: jen, check):
    try:
        jen.get_job_info(check)
        return True
    except NotFoundException:
        pass
    except Exception as e:
        l.fatal(e)
    return False


def _need_update(job):
    # Check job version string in job config here off of version in settings, if differ - flag for update.
    return True


def _sub_master(master, job):
    config = master.replace(
        'Master_1', job.name).replace(
        'default_codeline', job.codeline).replace(
        'default_release', job.release).replace(
        'default_notify', job.notify
    )
    return config


def _get_config(jen: jen, master: str):
    try:
        if master.endswith('.xml'):
            # Get config from VCS root
            config_file = '{}/../jobs/{}'.format(os.path.dirname(os.path.realpath(__file__)), master)
            with open(config_file, 'r+') as f:
                config = f.read()
        else:
            # Get config from job
            config = jen.get_job_config(master)
    except e:
        l.exception(f'Failed to get job {master} config')

    return config


def audit_jobs(jen: jen, jobs, debug=True):
    l.info(jobs)
    for job in jobs:
        l.info(job)
        job_path = f"{job.folder}{job.name}"
        l.info(f'Auditing {job_path}')
        new_config = _sub_master(_get_config(jen, job.master), job)
        if not _job_exists(jen, job_path):
            l.info(f'Must create: {job_path}')
            if not debug: jen.create_job(job_path, new_config)
        else:
            l.info('Job exists - Check if we need to update and update if needed.')
            if _need_update(job):
                l.info('Reconfiguring...')
                if not debug: jen.reconfig_job(job_path, new_config)


if __name__ == "__main__":
    pass
