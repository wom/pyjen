import jenkins
import argparse
import yaml
from pprint import pformat
from collections import namedtuple
import dem.slaves as slaves
import dem.jobs as jobs
import dem.plugins as plugins
import logging, sys

l = logging.getLogger('demo')


def parse_args():
    parser = argparse.ArgumentParser(description='Process some integers.')
    steps = ['jobs', 'plugins', 'slaves']
    parser.add_argument(
        '-s', '--steps', action='store', help='Steps to perform.', choices=steps, default=steps, type=str, nargs='*'
    )
    levels = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
    parser.add_argument(
        '-l', '--loglevel', help='Log Level', action='store', default='DEBUG', choices=levels
    )
    parser.add_argument('-d', '--debug', action='store_true', default=False)

    args = parser.parse_args()
    if args.debug:
        args.loglevel = 'NOTSET'
    return args


def setup_logging(level='DEBUG'):
    level = level.upper()
    if level not in ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'NOTSET']:
        level = 'INFO'
    logging.basicConfig(level=getattr(logging, level))


def main(args):
    def _convert(dictionary):
        for key, value in dictionary.items():
            if isinstance(value, dict):
                dictionary[key] = _convert(value)
        return namedtuple('Settings', dictionary.keys())(**dictionary)

    with open("settings.yaml", "r") as f:
        settings = _convert(yaml.load(f))
    jen = settings.jenkins
    l.info(f'Connecting to {jen.host} as {jen.user}')
    server = jenkins.Jenkins(jen.host, username=jen.user, password=jen.token)
    if 'plugins' in args.steps:
        l.info('Verifying Plugins..')
        restarted = plugins.audit_plugins(server, settings.plugins, debug=args.debug)
        if restarted:
            l.info('Reconnecting after restart..')
            server = jenkins.Jenkins(jen.host, username=jen.user, password=jen.token, timeout=30)
            server.wait_for_normal_op(30)
    if 'slaves' in args.steps:
        l.info('Verifying Slaves..')
        slaves.audit_slaves(server, settings.slaves, debug=args.debug)
    if 'jobs' in args.steps:
        l.info('Verifying Jobs..')
        jobs.audit_jobs(server, settings.jobs, debug=args.debug)
    l.info('Complete')

if __name__ == "__main__":
    args = parse_args()
    print(args)
    setup_logging(args.loglevel)
    if args.debug:
        l.info('Debug Mode Enabled')
    main(args)
